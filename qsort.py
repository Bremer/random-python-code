""" quicksort implementation in python """
import random


def qsort(qslist):
    """ The actual quicksort algorithm """
    if len(qslist) < 2:
        return qslist
    big = []
    small = []
    pivot = random.choice(qslist)
    for item in qslist:
        if item <= pivot:
            small.append(item)
        elif item > pivot:
            big.append(item)
    if len(big) == 0:
        big.append(pivot)
        small.remove(pivot)
    return qsort(small) + qsort(big)


def main():
    """ Runs the quicksort algorithm with one testcase """
    qslist = [9,1,1,2,3,2,7,3,6,4,5] * 200
    print(qsort(qslist))


if __name__ == "__main__":
    main()
