""" State pattern implemented in pyhton as a stop watch """
#!/usr/bin/python3
import time
import sys


class WatchState():
    """ Contains logic for start and stop which is inherited by Stop and Start """
    name = "state"
    allowed = []
    start_time = 0.00
    stop_time = 0.00

    def switch(self, new_state):
        """ Switch state """
        if new_state.name in self.allowed:
            print(f'Current: {self} => switched to new state {new_state.name}')
            self.__class__ = new_state
            if new_state.name == "start":
                self.start_time = time.time()
            elif new_state.name == "stop":
                self.stop_time = time.time()
                elapsed_time = self.stop_time - self.start_time
                print(f'Elapsed time in seconds : {elapsed_time:.2f}')
        else:
            print(f'Current: {self} => switching to {state.name} not possible.')

    def __str__(self):
        return self.name


class Stop(WatchState):
    """ Contains state and allowed change """
    name = "stop"
    allowed = ['start']


class Start(WatchState):
    """ Contains state and allowed change """
    name = "start"
    allowed = ['stop']


class Watch():
    """ Performs change of state """
    def __init__(self):
        self.state = Stop()

    def change(self, new_state):
        """ New state """
        self.state.switch(new_state)


if __name__ == "__main__":
    stop_watch = Watch()
    for state in sys.stdin:
        state = state.strip("\n")
        if state == "start":
            stop_watch.change(Start)
        elif state == "stop":
            stop_watch.change(Stop)
        else:
            print("Invalid state, please try 'start' or 'stop' ")
